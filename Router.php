<?php

namespace ReeBase;

class Router
{

	/**
	 * Routes
	 * @var array
	 */
	protected $_routes = array();

	/**
	 * Get instance
	 *
	 * @return Router|false
	 */
	public function getInstance()
	{
		return Registry::getInstance('router', '\ReeBase\Router');
	}

	/**
	 * Initialize
	 *
	 * @param array $options
	 */
	public function __construct(array $options = array())
	{
		if (array_key_exists('routesfile', $options)) {
			$this->loadFromFile($options['routesfile']);
		}
	}

	/**
	 * Load routes from file
	 *
	 * @param string $filename Full path to file
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public function loadFromFile($filename)
	{
		if (!is_file($filename)) {
			throw new \Exception(sprintf('Could not find file "%s"', $filename));
		}

		$routes = array_merge($this->_routes, require $filename);

		foreach ($routes as $routeRegEx => $route) {

			$this->_routes[$this->_editRegEx($routeRegEx)] = $route;
		}

		return $this;
	}

	/**
	 * Will replace some of the 'readable' expression parts into actual expressions
	 *
	 * @param string $routeRegEx
	 *
	 * @return string
	 */
	protected function _editRegEx($routeRegEx)
	{
		$replace = array(
			'[num]' => '([0-9]+)',
			'[alpha]' => '([a-zA-Z]+)',
			'[alnum]' => '([0-9a-zA-Z]++)',
			'[any]' => '(.+)',
			'/' => '\\/'
		);

//		$routeRegEx = preg_quote($routeRegEx);
		return '/' . str_replace(
			array_keys($replace),
			array_values($replace),
			$routeRegEx
		) . '/';
	}

	/**
	 * Add a route
	 *
	 * @param string $regEx
	 * @param array $dispatchInfo
	 *
	 * @return $this
	 */
	public function addRoute($regEx, array $dispatchInfo)
	{
		$this->_routes[$this->_editRegEx($regEx)] = $dispatchInfo;

		return $this;
	}

	/**
	 * Check route and return new URI when applicable
	 *
	 * @param string $uri
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function checkRoute($uri)
	{
		$return = $uri;

		foreach ($this->_routes as $routeRegEx => $newRoute) {
			if (preg_match($routeRegEx, $uri) === 1) {
				$return = preg_replace($routeRegEx, $newRoute, $uri);
				break;
			} else {
				if (preg_last_error() !== 0) {
					throw new \Exception(sprintf('Regular expression fail: "%s"', $routeRegEx));
				}
			}
		}

		return $return;
	}

}