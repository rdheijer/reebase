<?php

namespace ReeBase\Form;

use ReeBase\Registry;
use ReeStyle\Dispatcher;

class Builder
{

	const ENDTYPE_NONE = 'none';
	const ENDTYPE_TAG = 'tag';
	const ENDTYPE_ISEND = 'isend';
	const ENDTYPE_ELEMENT = 'element';

	/**
	 * Form configuration
	 * @var null|\stdClass
	 */
	protected $_config = null;

	/**
	 * Data container
	 * @var array
	 */
	protected $_population = array();

	/**
	 * Initialize
	 *
	 * @param array $config
	 */
	public function __construct(array $config = array())
	{

	}

	/**
	 * Load configuration
	 *
	 * @param string $configFilename Name of config file
	 * @param string|null $configPath Path to config file
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public function load($configFilename, $configPath = null)
	{
		if (null !== $configPath) {
			/**
			 * @var Dispatcher $dispatcher
			 */
			$dispatcher = Registry::getInstance('dispatcher');
			$configPath = implode('/', array(
				APP_BASE,
				$dispatcher->getModule(),
				'forms',
				$dispatcher->getController()
			));
		}

		if (!is_dir($configPath)) {
			throw new \Exception(sprintf('"%s" is not a valid path.', $configPath));
		}

		$filePath = $configPath . '/' . $configFilename;
		if (!is_file($filePath)) {
			throw new \Exception(sprintf('"%s" is not a valid file.', $configFilename));
		}

		$config = require $filePath;

		if (!is_object($config)) {
			throw new \Exception('Parameter must be an object.');
		}

		$this->_config = $config;

		return $this;
	}

	/**
	 * Populate elements
	 *
	 * @param array $population
	 *
	 * @return $this
	 */
	public function populate(array $population)
	{
		$this->_population = $population;

		return $this;
	}

	/**
	 * Get data (population)
	 *
	 * @return array
	 */
	public function getPopulation()
	{
		return $this->_population;
	}

	/**
	 * Insert form starting tag
	 *
	 * @return string
	 */
	public function insertStart()
	{
		return $this->_createHtml('form', array());
	}

	/**
	 * Insert controls
	 *
	 * @param null|string $controls
	 * @param string $separator
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function insertControls($controls = null, $separator = '|')
	{
		if (null === $controls) {
			$controls = array_keys((array)$this->_config);
		} elseif (is_string($controls)) {
			$controls = explode($separator, $controls);
		} elseif (!is_array($controls)) {
			throw new \Exception('Parameter must be either a pipe-delimited string or an array.');
		}

		$return = array();
		foreach ($controls as $control) {
			$return[] = $this->getControl($control);
		}

		return implode(PHP_EOL, $return);
	}

	/**
	 * @param $control
	 *
	 * @return string
	 */
	public function getControl($control)
	{
		$controlHtml = '';

		return $controlHtml;
	}

	/**
	 * Insert form end
	 *
	 * @return string
	 */
	public function insertEnd()
	{
		return $this->_createHtml('form', array(), static::ENDTYPE_ISEND);
	}

	/**
	 * Create a tag
	 *
	 * @param string $tag
	 * @param array $attributes
	 * @param string $content
	 * @param string $endType
	 *
	 * @return string
	 */
	protected function _createHtml($tag, array $attributes, $content = '', $endType = self::ENDTYPE_NONE)
	{
		$html = '<' . $tag;

		$htmlAttr = array();
		if (count($attributes) > 0) {
			foreach ($attributes as $name => $value) {
				$htmlAttr[] = sprintf('%s=""', $name, htmlentities($value));
			}
			$html .= ' ' . implode(' ', $attributes);
		}

		switch ($endType) {
			case static::ENDTYPE_ELEMENT:
				$html .= ' />';
				break;

			case static::ENDTYPE_TAG:
				$html .= $content . '</' . $tag . '>';
				break;

			case static::ENDTYPE_ISEND:
				$html = $html[0] . '/' . substr($html, 1) . '>';
				break;

			default:
				$html .= '>';
				break;
		}

		return $html;
	}

}