<?php

namespace ReeBase\Skeletons;

interface ViewSkeleton
{

	/**
	 * Generic setter
	 *
	 * @param $var
	 * @param $val
	 *
	 * @return ViewSkeleton
	 */
	public function set($var, $val);

	/**
	 * Set view location
	 *
	 * @param $viewLocation
	 *
	 * @return ViewSkeleton
	 * @throws \Exception
	 */
	public function setViewLocation($viewLocation);


	/**
	 * Set script name
	 *
	 * @param null|string $scriptName
	 *
	 * @return ViewSkeleton
	 */
	public function setScriptName($scriptName);

	/**
	 * Render view
	 *
	 * @param null|string $template
	 * @param bool $returnOnly
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function render($template = null, $returnOnly = false);

}