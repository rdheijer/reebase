<?php

namespace ReeBase;

use ReeBase\Skeletons\AuthAdapterSkeleton as Skeleton;

/**
 * Class Auth
 *
 * @package ReeBase
 */
class Auth
{

	/**
	 * Is authenticated
	 * @var bool
	 */
	protected $_authenticated = false;

	/**
	 * Adapter
	 * @var null|Skeleton
	 */
	protected $_adapter = null;

	/**
	 * Initialize
	 */
	public function __construct()
	{
		$this->wakeup();
	}

	/**
	 * Wake up the adapter
	 *
	 * @return Auth
	 */
	public function wakeup()
	{
		$this->_adapter->wakeup();

		return $this;
	}

	/**
	 * Set adapter to use
	 *
	 * @param Skeleton $adapter
	 *
	 * @return Auth
	 */
	public function setAdapter(Skeleton $adapter)
	{
		$this->_adapter = $adapter;

		return $this;
	}

	/**
	 * Put the auth to sleep
	 *
	 * @return Auth
	 */
	public function sleep()
	{
		$this->_adapter->sleep();

		return $this;
	}

	/**
	 * Authenticate a user
	 *
	 * @param $login
	 * @param $password
	 *
	 * @return bool
	 */
	public function authenticate($login, $password)
	{
		return $this->_adapter->authenticate($login, $password);
	}

	/**
	 * Is user still authenticated
	 *
	 * @return mixed
	 */
	public function authenticated()
	{
		return $this->_adapter->authenticated();
	}

	/**
	 * Happening before destroy of the object
	 */
	public function __destroy()
	{
		$this->sleep();
	}

}