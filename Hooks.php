<?php

namespace ReeBase;

class Hooks
{

	static private $_instance = null;

	protected $_hooks = array();

	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function register($hookName, $callback, $params = null)
	{
		$this->_hooks[$hookName][] = (object)array(
			'callback' => $callback,
			'params' => $params
		);

		return $this;
	}

	public function run($hookName, $defaultReturn = null)
	{
		if (array_key_exists($hookName, $this->_hooks)) {
			$hook = $this->_hooks[$hookName];

			$return = null !== $defaultReturn ? $defaultReturn : null;
			foreach ($hook as $index => $catch) {
				// example: function name(Controller $controller, &$return, &$params, $index) {}
				$return = call_user_func($catch->callback, Controller::getInstance(), $return, $catch->params, $index);
			}
		} else {
			$return = $defaultReturn;
		}

		return $return;
	}

}