# README #

This is a new try at a HMVC framework (using modules as hierarchy), where you can implement your own ORM/DBAL. It has a View, Controller, Registry and Config object with what you can do most of the configuration. Just about everything is working, though probably not yet loaded with functionality. There is, however, a possibility to implement your own View class, as long as it implements the ViewSkeleton.

It is not yet ready for large scaled enterprise solutions (no LDAP or other authentication layers are included, nor is SOAP - thoguh you can surely make your own).

It is working with a 'PATH_INFO' approach: index.php/module/controller/action/param1/param2...

If you need some basic code on how to get started, just leave a message...
