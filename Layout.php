<?php

namespace ReeBase;

use ReeBase\Skeletons\ViewSkeleton as ViewSkeleton;

/**
 * Class Layout
 *
 * @package ReeBase
 */
class Layout
{

	/**
	 * Internal View script name
	 * @var null|string
	 */
	protected $_layoutScript = null;

	/**
	 * Internal View script name
	 * @var null|Skeletons\ViewSkeleton
	 */
	protected $_viewClassName = null;

	/**
	 * Internal View instance
	 * @var null|Skeletons\ViewSkeleton
	 */
	protected $_view = null;

	/**
	 * Layout view data
	 * @var array
	 */
	protected $_layoutViewData = array();

	/**
	 * Holds views
	 * @var array
	 */
	protected $_viewList = [];

	/**
	 * Layout may render
	 * @var bool
	 */
	protected $_canRender = true;

	/**
	 * Initialize
	 */
	public function __construct(array $options = array())
	{
		$this->setOptions($options);
	}

	/**
	 * Set options by array
	 *
	 * @param array $options
	 *
	 * @return Layout
	 */
	public function setOptions(array $options)
	{
		foreach ($options as $optionName => $setting) {
			$method = 'set' . $optionName;
			if (method_exists($this, $method)) {
				call_user_func(array($this, $method), $setting);
			}
		}

		return $this;
	}

	/**
	 * @param ViewSkeleton $view
	 *
	 * @return Layout
	 */
	public function add(ViewSkeleton $view)
	{
		array_push($this->_viewList, $view);

		return $this;
	}

	/**
	 * Set layout script name
	 *
	 * @param string $layoutScript
	 *
	 * @return Layout
	 */
	public function setLayoutScript($layoutScript)
	{
		$this->_layoutScript = $layoutScript;

		return $this;
	}

	/**
	 * Set viewclass
	 *
	 * @param string $viewClass
	 *
	 * @return Layout
	 */
	public function setViewClass($viewClass)
	{
		$this->_viewClassName = $viewClass;

		return $this;
	}

	/**
	 * Set page title
	 *
	 * @param string $pageTitle
	 *
	 * @return $this
	 */
	public function setPageTitle($pageTitle)
	{
		$this->_layoutViewData['title'] = (string)$pageTitle;

		return $this;
	}

	/**
	 * Set page title
	 *
	 * @return string|null
	 */
	public function getPageTitle()
	{
		return array_key_exists('title', $this->_layoutViewData) ? $this->_layoutViewData['title'] : '';
	}

	/**
	 * Can layout be rendered
	 *
	 * @return bool
	 */
	public function canRender()
	{
		return $this->_canRender;
	}

	/**
	 * Cancel layout render
	 *
	 * @return Layout
	 */
	public function cancelRender()
	{
		$this->_canRender = false;

		return $this;
	}

	/**
	 * Render layout
	 *
	 * @param bool $returnOnly
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function render($returnOnly = false)
	{
		Hooks::getInstance()->run('before_layoutrender');

		$class = Config::getInstance()->global->views->class;

		$this->_view = new $class();

		if (!($this->_view instanceof ViewSkeleton)) {
			throw new \Exception('View instance must implement ReeBase\Skeletons\ViewSkeleton', 500);
		}

		$this->_view->title = $this->getPageTitle();

		$content = array();
		/**
		 * @var ViewSkeleton $view
		 */
		foreach ($this->_viewList as $view) {
			$content[] = $view->render(null, true);
		}

		$this->_view->output = implode(PHP_EOL, $content);

		$this->_view->setViewLocation(
			Config::getInstance()->global->views->layout->path
		);

		$return = $this->_view->render($this->_layoutScript, $returnOnly);
		Hooks::getInstance()->run('after_layoutrender');

		return $return;
	}

}