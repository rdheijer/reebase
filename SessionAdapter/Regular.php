<?php

namespace ReeBase\SessionAdapter;

use ReeBase;

ReeBase\Load::skeleton('Session');

use ReeBase\Skeletons\SessionSkeleton as Skeleton;

/**
 * Default behaviour using whatever is configured in PHP using the $_SESSION superglobal
 *
 * @package ReeBase\SessionAdapter
 */
class Regular implements Skeleton
{

	/**
	 * Start session
	 *
	 * @return $this
	 */
	public function start()
	{
		session_start();

		return $this;
	}

	/**
	 * Stop session
	 *
	 * @return $this|mixed
	 */
	public function destroy()
	{
		session_destroy();

		return $this;
	}

	/**
	 * Set a setting
	 *
	 * @param $var
	 * @param $val
	 *
	 * @return $this
	 */
	public function set($var, $val)
	{
		$_SESSION[$var] = $val;

		return $this;
	}

	/**
	 * Get a setting
	 *
	 * @param $var
	 * @param null $default
	 *
	 * @return mixed|null
	 */
	public function get($var, $default = null)
	{
		return array_key_exists($var, $_SESSION) ? $_SESSION[$var] : $default;
	}

	/**
	 * Forget/unset a setting
	 *
	 * @param $var
	 *
	 * @return bool
	 */
	public function forget($var)
	{
		unset($_SESSION[$var]);

		return $this;
	}

}