<?php

namespace ReeBase\Skeletons;

/**
 * Interface SessionSkeleton
 *
 * @package ReeBase\Skeletons
 */
interface SessionSkeleton
{

	/**
	 * Start session
	 *
	 * @return mixed
	 */
	public function start();

	/**
	 * Stop session
	 *
	 * @return mixed
	 */
	public function destroy();

	/**
	 * Get session variable
	 *
	 * @param $var
	 * @param null|mixed $default
	 *
	 * @return mixed
	 */
	public function get($var, $default = null);

	/**
	 * Set a session variable
	 *
	 * @param $var
	 * @param $val
	 *
	 * @return mixed
	 */
	public function set($var, $val);

	/**
	 * Forget/unset a session variable
	 *
	 * @param $var
	 *
	 * @return mixed
	 */
	public function forget($var);

}