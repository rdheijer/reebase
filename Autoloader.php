<?php

/**
 * PSR Compatible autoloader
 *
 * Class Autoloader
 */


class Autoloader
{

    static public function register()
    {
        $autoloader = new self();

        spl_autoload_register(array($autoloader, 'load'));

        return $autoloader;
    }

    public function addPath($path)
    {
        $current = get_include_path();
    }

    public function load($className)
    {
        list($vendor, $relativePath) = explode('\\', $className, 2);

        \ReeBase\Load::source($vendor, str_replace('\\','/', $relativePath));
    }

}