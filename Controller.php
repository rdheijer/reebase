<?php

namespace ReeBase;

/**
 * Class Controller
 *
 * @package ReeBase
 */
abstract class Controller
{

	/**
	 * Global instance that contains current controller
	 * @var null|\ReeBase\Controller
	 */
	static private $_instance = null;

	/**
	 * Internal view
	 * @var null|\ReeBase\View
	 */
	public $view = null;

	/**
	 * @var bool
	 */
	protected $_renderAction = true;

	/**
	 * Page title
	 * @var string
	 */
	protected $_pageTitle = '';

	/**
	 * Get current active instance
	 *
	 * @return null|Controller
	 */
	public static function getInstance()
	{
		return self::$_instance;
	}

	/**
	 * Set instance
	 *
	 * @param $instance
	 */
	public static function setInstance($instance)
	{
		self::$_instance = $instance;
	}

	/**
	 * Setup controller
	 */
	public function setup()
	{
		/**
		 * Let the environment know this is the active controller
		 */
		Controller::setInstance($this);

		/**
		 * Setup the view class
		 */
		$viewInfo = Config::getInstance()->global->views;
		$viewClass = $viewInfo->class;
		$this->view = new $viewClass();

		/**
		 * We need to know the dispatcher version of the controller name... And remove the sufix
		 */
		$suffix = Config::getInstance()->global->controllers->suffix;
		$controller = Dispatcher::getInstance()->getController();

		$this->view->setViewLocation(
			strtolower(Dispatcher::getInstance()->getModuleLocation())
			. Config::getInstance()->global->locations->views
			. '/' . strtolower(
				substr(
					$controller,
					0, strrpos($controller, $suffix)
				)
			)

		);

//		$this->view->user = \ReeBase\Registry::getInstance('auth')->authenticated();
	}

	/**
	 * Forward to next action (controller/module not yet implemented)
	 *
	 * @param $action
	 * @param array $params
	 *
	 * @return $this
	 * @throws \Exception
	 */
	protected function _forward($action, array $params = array())
	{
		$currentAction = Dispatcher::getInstance()->getAction();

		Dispatcher::getInstance()->setAction($action);

		if (!method_exists($this, $action)) {
			throw new \Exception(vsprintf('Cannot find action "%s" in controller "%s".', array($action, get_class($this))));
		}

		call_user_func(array($this, $action), $params);

		Dispatcher::getInstance()->setAction($currentAction);

		return $this;
	}

	/**
	 * Render according view
	 *
	 * @param null|string $viewScript
	 *
	 * @return string
	 */
	public function render($viewScript = null)
	{
		return $this
			->getView(
				(null === $viewScript ? lcfirst(Dispatcher::getInstance()->getAction()) : $viewScript)
			)
			->render();
	}

	/**
	 * Get page title
	 *
	 * @return string
	 */
	public function getPageTitle()
	{
		return $this->_pageTitle;
	}

	/**
	 * Get view object
	 *
	 * @param $viewScript
	 *
	 * @return null|View
	 */
	public function getView($viewScript)
	{
		$dispatcher = Dispatcher::getInstance();

		$this->view->setViewLocation(implode('/', array(
			strtolower($dispatcher->getModuleLocation()),
			'views',
			strtolower(lcfirst($dispatcher->getController()))
		)));

		$this->view->setScriptName($viewScript = null);

		return $this->view;
	}

	/**
	 * Tear down controller
	 */
	public function tearDown()
	{
		Controller::setInstance(null);
	}

}