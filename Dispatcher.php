<?php

namespace ReeBase;

/**
 * Class Dispatcher
 *
 * @package ReeBase
 */
class Dispatcher
{

	/**
	 * Instance of myself
	 * @var Dispatcher
	 */
	static protected $_instance;

	/**
	 * Bsae URI (e.g. for creating URL's)
	 * @var null
	 */
	protected $_baseUri = null;

	/**
	 * Dispatch parts
	 * @var array
	 */
	protected $_dispatch = array();

	/**
	 * @var null|Layout
	 */
	protected $_layout = null;

	/**
	 * Action information
	 * @var array|object
	 */
	protected $_current = array(
		'action' => 'index',
		'controller' => 'Index',
		'module' => 'standard',
		'controllerLocation' => null
	);

	/**
	 * Get instance
	 *
	 * @return Dispatcher
	 */
	public static function getInstance()
	{
		if (null === self::$_instance) {
			$staticBind = get_called_class();
			static::$_instance = new $staticBind();
		}

		return self::$_instance;
	}

	/**
	 * Initialize
	 */
	public function __construct()
	{
		$this->_current = (object)$this->_current;

		$this->_setup();
	}

	/**
	 * Setup dispatcher
	 */
	protected function _setup()
	{
		$layoutConfig = Config::getInstance()->global->views->layout;
		if ($layoutConfig->enable) {
			$this->_layout = new Layout(array(
				'ViewClass' => $layoutConfig->path,
				'LayoutScript' => $layoutConfig->default
			));
		}

		$this->_baseUri = $_SERVER['SCRIPT_NAME'];

		/**
		 * @var Router $router
		 */
		$router = Registry::getInstance('router');
		if (array_key_exists('PATH_INFO', $_SERVER)) {
			$uri = trim($_SERVER['PATH_INFO'], ' /');

			if ($router instanceof Router) {
				$uri = $router->checkRoute($uri, array());
			}

			$parts = explode('/', $uri);

			$errLevel = error_reporting(0);
			$dispatchList = array('module', 'controller', 'action');
			foreach ($dispatchList as $index => $dispatchPart) {
				$$dispatchPart = array_key_exists($index, $parts) ? $parts[$index] : null;
			}

			$params = array_slice($parts, 3);
			error_reporting($errLevel);

			$module = empty($module) ? 'standard' : strtolower($module);
			$controller = empty($controller) ? 'Index' : ucfirst(strtolower($controller));
			$action = empty($action) ? 'index' : strtolower($action);
		} else {
			$params = array();
			$module = 'standard';
			$controller = 'Index';
			$action = 'index';
		}

		$this->add($action, $controller, $module, $params);
	}

	/**
	 * Get base URI
	 *
	 * @return null|string
	 */
	public function getBaseUri()
	{
		return $this->_baseUri;
	}

	/**
	 * @param string  $action
	 * @param null $controller [optional]
	 * @param null $module [optional]
	 * @param array $params [optional]
	 *
	 * @return Dispatcher
	 */
	public function add($action, $controller = null, $module = null, $params = array())
	{
		$module = null === $module ? $this->_current->module : $module;
		$controller = (null === $controller ? 'Index' : $controller);

		$this->_dispatch[] = (object)array(
			'action' => $action,
			'controller' => $controller,
			'module' => $module,
			'params' => $params,
		);

		return $this;
	}

	/**
	 * Set action
	 *
	 * @param string $action
	 *
	 * @return Dispatcher
	 */
	public function setAction($action)
	{
		$this->_current->action = $action;

		return $this;
	}

	/**
	 * Get action
	 *
	 * @return string
	 */
	public function getAction()
	{
		return $this->_current->action;
	}

	public function getController()
	{
		return $this->_current->controller;
	}

	public function getModule()
	{
		return $this->_current->module;
	}

	public function getModuleLocation()
	{
		return APP_BASE .  '/modules/' . $this->_current->module;
	}

	public function getlayout()
	{
		return $this->_layout;
	}

	public function run()
	{
		$reservedActions = $this->getReservedActions();

		reset($this->_dispatch);
		while ($dispatch = current($this->_dispatch)) {

			$this->runHandler($dispatch, $reservedActions);

			next($this->_dispatch);
		}

		if ($this->_layout->canRender()) {
			$this->_layout->render();
		}
	}

	public function getReservedActions()
	{
		$reflector = new \ReflectionClass('\ReeBase\Controller');

		$reservedActions = array();

		/**
		 * @var \ReflectionMethod $methodObject
		 */
		foreach ($reflector->getMethods() as $methodObject) {
			$reservedActions[] = $methodObject->name;
		}

		return $reservedActions;
	}

	public function runHandler($dispatch, $reservedActions = array())
	{

		if (in_array($dispatch->action, $reservedActions)) {
			throw new \Exception(
				sprintf('You tried to invoke a reserved action (%s)', $dispatch->action),
				500
			);
		}

		$controllerLocation = Load::controller(
			$dispatch->module,
			$dispatch->controller
		);

		$className =
			'Application\\Modules\\' . ucfirst($dispatch->module) . '\\' . $dispatch->controller
			. Config::getInstance()->global->controllers->suffix;

		/**
		 * @var \ReeBase\Controller $controllerObject
		 */
		$controllerObject = new $className();

		if (!method_exists($controllerObject, $dispatch->action)) {
			throw new \Exception(
				vsprintf('Could not find action "%s" in controller "%s"', array($dispatch->action, $className))
			);
		}

		// Set current controller
		$this->_current->action = $dispatch->action;
		$this->_current->controller = $dispatch->controller;
		$this->_current->module = $dispatch->module;
		$this->_current->controllerLocation = $controllerLocation;

		// Register controller
		call_user_func(array($className, 'setInstance'), $controllerObject);

		// Setup
		$controllerObject->setup();

		// Run
		call_user_func_array(array($controllerObject, $dispatch->action), $dispatch->params);

		$pageTitle = $this->_layout->getPageTitle();
		if (empty($pageTitle)) {
			$this->_layout->setPageTitle(
				$controllerObject->getPageTitle()
			);
		}

		// Add to rendering
		$this->_layout->add(
			$controllerObject->getView($dispatch->action)
		);

		// Teardown
		$controllerObject->tearDown();

	}

}