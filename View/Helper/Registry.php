<?php

namespace ReeBase\View\Helper;

use ReeBase;

\ReeBase\Load::skeleton('ViewHelper');

use ReeBase\Skeletons\ViewHelperSkeleton as Skeleton;

/**
 * Class Url
 *
 * @package ReeBase\View\Helper
 */
class Registry implements Skeleton
{

	/**
	 * @var null|\ReeBase\View
	 */
	protected $_view = null;

	/**
	 * Initialize
	 *
	 * @param ReeBase\View $view
	 */
	public function __construct(ReeBase\View $view)
	{
		$this->_view = $view;
	}

	/**
	 * Invoke
	 *
	 * @param mixed $params
	 *
	 * @return string
	 */
	public function invoke($params)
	{
		if (array_key_exists(0, $params)) {
			return \ReeBase\Registry::getInstance($params[0]);
		}

		return new stdClass;
	}

}