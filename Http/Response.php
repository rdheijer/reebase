<?php

namespace ReeBase\Http;

use ReeBase\Registry;
use ReeStyle\Dispatcher;

class Response
{

	const CONTENT_APPLICATION_JSON = 'application/json';
	const CONTENT_XML = 'text/xml';
	const CONTENT_HTML = 'text/html';
	const CONTENT_XHTML = 'text/xhtml';
	const CONTENT_SOAP1_1 = self::CONTENT_XML;
	const CONTENT_SOAP1_2 = 'application/soap+xml';

	/**
	 * Charset to send along with a content-type header
	 * @var string
	 */
	protected $_charset = 'utf-8';

	public function __construct()
	{

	}

	/**
	 * Tell the response it's
	 */
	public function setAjaxHeader()
	{
		$this->setHeader(self::CONTENT_APPLICATION_JSON);

		return $this;
	}

	/**
	 * Set expiry headers
	 *
	 * @param integer $time Tim in seconds since Unix Epoch - 0 is first date ever in Unix world
	 *
	 * @return $this
	 */
	public function setExpired($time = 0)
	{
		$this->setHeader('Expires', gmdate('D, d M Y H:i:s \G\M\T', $time));
		$this->setHeader('Cache-Control', 'no-cache, must-revalidate');

		return $this;
	}

	/**
	 * Set character set - unless your country demands something other than UTF-8 (or similar), do not change!!
	 *
	 * @param string $charset
	 *
	 * @return $this
	 */
	public function setCharset($charset)
	{
		$this->_charset = $charset;

		return $this;
	}

	/**
	 * Set content type header along with set charset
	 *
	 * @param string $contentType
	 *
	 * @return $this
	 */
	public function setContentType($contentType)
	{
		$charSet = '; charset=' . $this->_charset;

		$this->setHeader('Content-type', $contentType . $charSet);

		return $this;
	}

	/**
	 * Set the header
	 *
	 * @param string $header
	 * @param null|string $value
	 *
	 * @return $this
	 */
	public function setHeader($header, $value = null)
	{
		$value = $value !== null ? ': ' . $value : '';

		header($header . $value);

		return $this;
	}

	public function redirectToAction($action, $controller = null, $module = null, $params = array())
	{
		$dispatcher = Registry::getInstance('dispatcher');

		$uriParts = array(
			is_string($module) ? $module : $dispatcher->getModule(),
			is_string($controller) ? $controller : $dispatcher->getController(),
			$action
		);

		$uriAction = implode('/', $uriParts);
		$uriParams = implode('/', $params);

		$baseUri = $dispatcher->getBaseUri();
		$fullUri = $baseUri . '/' . $uriAction . '/' . $uriParams;

		$this->redirect($fullUri);
	}

	public function redirect($url, $redirectCode = 303)
	{
		$this->setHeader('HTTP 1.1 ' . $redirectCode . ' See Other');
		$this->setHeader('Location', $url);

		exit;
	}

}