<?php

namespace ReeBase;

/**
 * Class Rbac (Role-Based Access Control)
 *
 * @package ReeBase
 */
class Rbac
{

	/**
	 * Instance
	 * @var null
	 */
	static protected $_instance = null;

	/**
	 * Default access
	 * @var int
	 */
	protected $_defaultAccess = 0;

	/**
	 * Role access
	 * @var array
	 */
	protected $_roleAccess = array();

	/**
	 * Get instance
	 *
	 * @param array $roleAccess
	 *
	 * @return null|Rbac
	 */
	static public function getInstance(array $roleAccess = array())
	{
		null === static::$_instance && (static::$_instance = new static($roleAccess));

		return static::$_instance;
	}

	/**
	 * Initialize
	 *
	 * @param array $roleAccess
	 */
	public function __construct(array $roleAccess = array())
	{
		$this->setUser($roleAccess);
	}

	/**
	 * Set role to control access permission
	 *
	 * @param string $role
	 * @param string $control
	 * @param int $privileges
	 *
	 * @return $this
	 */
	public function setRoleAccess($role, $control = null, $privileges = null)
	{
		if (is_array($role)) {
			$this->_roleAccess = $role;
		} else {
			$privileges = $privileges === null || !is_int($privileges) ? $this->_defaultAccess : $privileges;

			$this->_roleAccess[$role][$control] = $privileges;
		}

		return $this;
	}

	/**
	 * Is user allowed
	 *
	 * @param string $role
	 * @param string $control
	 * @param string $privileges
	 *
	 * @return bool
	 */
	public function isAllowed($role, $control, $privileges = null)
	{
		$roleAccess = false;

		if (array_key_exists($role, $this->_roleAccess)) {
			if (array_key_exists($control, $this->_roleAccess[$role])) {
				$controlPrivileges = $this->_roleAccess[$role][$control];

				if ($privileges > 0) {
					$roleAccess = ($privileges | $controlPrivileges) > 0;
				} else {
					$roleAccess = true;
				}
			}
		}

		return $roleAccess;
	}

	/**
	 * Is user not allowed
	 *
	 * @param string $control
	 * @param string $tokens
	 *
	 * @return bool
	 */
	public function isDisallowed($control, $tokens = null)
	{
		return !$this->isAllowed($control);
	}

}