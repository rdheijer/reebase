<?php

namespace ReeBase;

class ErrorHandler
{

	public static function setup()
	{
		set_exception_handler('\ReeBase\ErrorHandler::handleException');
		set_error_handler('\ReeBase\ErrorHandler::handleError');
	}

	/**
	 * Handle error
	 */
	public static function handleError($severity, $message)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');

		$code = 500;
		$class = \get_called_class();

		$traceLines = array();
		foreach (debug_backtrace() as $index => $trace) {
			$trace['file'] = array_key_exists('file', $trace) ? $trace['file'] : false;
			$trace['line'] = array_key_exists('line', $trace) ? $trace['line'] : false;
			$traceLines[] = sprintf('#%s %s at %s', $index, $trace['file'], $trace['line']);
		}
		$traceLines = implode(PHP_EOL, $traceLines);

		ob_start();
		require APP_BASE . '/modules/standard/views/error.phtml';
		print ob_get_clean();
	}

	/**
	 * Handle exception
	 */
	public static function handleException(\Exception $exception)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');

		$message = $exception->getMessage();
		$line = $exception->getLine();
		($code = $exception->getCode()) || ($code = 500);
		$trace = $exception->getTrace();
		$traceLines = (string)$exception;

		ob_start();
		if (Config::getInstance()->global) {
			require Config::getInstance()->global->views->layout->path . '/exception.phtml';
		} else {
			print <<<HTML
<pre>
{$traceLines}
</pre>
HTML;
		}
		print ob_get_clean();

	}

}