<?php

namespace ReeBase\Skeletons;

interface AuthAdapterSkeleton
{

	public function wakeup();

	public function sleep();

	public function authenticate($login, $password);

	public function authenticated();

}