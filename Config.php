<?php

namespace ReeBase;

/**
 * Class Config
 *
 * @package ReeBase
 */
class Config
{

	/**
	 * Config instance
	 * @var null|Config
	 */
	static private $_instance = null;

	/**
	 * Data container
	 * @var mixed|null
	 */
	protected $_data = null;

	/**
	 * Get instance - singleton
	 *
	 * @param array|null $config
	 *
	 * @return Config
	 */
	public static function getInstance($config = null)
	{
		if (null === self::$_instance) {
			self::$_instance = new self($config);
		}

		return self::$_instance;
	}

	/**
	 * Initialize
	 *
	 * @param null|array $config
	 */
	public function __construct($config = null)
	{
		$this->_data = new \stdClass();

		if (is_array($config)) {
			if (array_key_exists('config_file', $config) && is_string($config['config_file'])) {
				$this->readConfigFile($config['config_file']);
			}
		}
	}

	/**
	 * Reads the contents of a config file
	 *
	 * @param $path
	 *
	 * @return Config
	 * @throws \Exception
	 */
	public function readConfigFile($path)
	{
		if (!is_file($path)) {
			throw new \Exception(sprintf('Cannot find "%s"', $path));
		}

		$this->_data = require $path; //require APP_BASE . '/config.php';

		return $this;
	}

	/**
	 * Magic getter
	 *
	 * @param $var
	 *
	 * @return bool|mixed
	 */
	public function __get($var)
	{
		return property_exists($this->_data, $var) ? $this->_data->$var : false;
	}

}